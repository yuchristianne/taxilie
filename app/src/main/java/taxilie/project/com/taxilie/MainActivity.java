package taxilie.project.com.taxilie;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    private Button report;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        report = (Button) findViewById(R.id.btnReport);


        report.setOnClickListener(new View.OnClickListener(){

                                      @Override
                                      public void onClick(View v) {
                                          Intent intent = new Intent(v.getContext(), ReportActivity.class);
                                          startActivity(intent);

                                      }
                                  });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
//
//    public void reportbtn (View view){
//
//        Intent intent = new Intent(this, ReportActivity.class);
//        startActivity(intent);
//    }
}
